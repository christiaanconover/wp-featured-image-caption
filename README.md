# Featured Image Caption
[![pipeline status](https://gitlab.com/christiaanconover/wp-featured-image-caption/badges/master/pipeline.svg)](https://gitlab.com/christiaanconover/wp-featured-image-caption/-/commits/master)

Featured Image Caption makes it simple to show a caption with the featured image of a post or page. It works seamlessly with most themes, with no coding required. If you like to mess about in the code, it supports that too.

## Installation
1. Upload the `featured-image-caption` directory to your site's `plugins` directory.
2. Activate the plugin through the `Plugins` menu in WordPress.
3. Review the plugin options found in `Settings` > `Featured Image Caption`.

## Documentation
All plugin documentation can be found on the [wiki](https://gitlab.com/christiaanconover/wp-featured-image-caption/-/wikis/home).
